<?php

/**
 * @file
 * Administration page callbacks for the MindMeister module.
 */

/**
 * Form builder. Configure MindMeister.
 *
 * @ingroup forms
 * @see system_settings.form().
 */
function mindmeister_admin_settings() {
  $form['api_keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('MindMeister API Keys'),
    '#description' => t(
      'Please enter the pair of API Keys for your MindMeister application.  If you have none, please go to !mindmeister and get yours.',
      array('!mindmeister' => l('MindMeister website', 'http://www.mindmeister.com/services/api/add_key'))
    ),
  );
  
  $form['api_keys']['mindmeister_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('Please enter your API Key value.'),
    '#default_value' => variable_get('mindmeister_api_key', NULL),
    '#required'=> TRUE,
  );
  
  $form['api_keys']['mindmeister_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret'),
    '#description' => t('Please enter your Secret value.'),
    '#default_value' => variable_get('mindmeister_secret', NULL),
    '#required'=> TRUE,
  );
    
  /*
  // The feature of storing map data locally is disabled at this time.
  // ToDo
  // Allows the user to choose this option.
  $form['mindmeister_mode'] = array(
    '#type' => 'select',
    '#title' => t('Implementation mode'),
    '#description' => t('Select the mode for MindMeister Implementation.<br />In the <strong>Local</strong>ss data mode, you probably cannot edit data simultaneously with your colleagues, but there is no limitation on the number of mindmaps you can create.  With <strong>Data on the cloud</strong> mode, you can work with your colleagues at the same time, but you can create a limited number of mindmaps unless you upgrade to one of paid accounts.'),
    '#default_value' => variable_get('mindmeister_mode', 'cloud'),
    '#required' => TRUE,
    '#options' => array(
      'local' => 'Local data',
      'cloud' => 'Data on the cloud',
    ),
  );
  */
  
  // The submit array may not be necessary, because system_settings_form() seems to add "Save configuration" button automatically.
  // $form['submit'] = array(
  //   '#type' => 'submit',
  //   '#value' => t('Submit'),
  // );
  
  return system_settings_form($form);
}