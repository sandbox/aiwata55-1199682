/**
 * Display mindmeister browser in a modal window.
 *
 * ToDo
 * - Keep the modal window at the center all the time as the window size is being changed.
 */
jQuery(document).ready(function() {
  jQuery(".mapeditor").overlay({
    mask: {
      color: '#333',
      // opacity: '0.9',
    },
    
    // load it immediately after the construction
    load: true,
  });
});